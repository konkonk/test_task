# 0. Functionality:
  * First;

# 1. Prerequisites for running locally:
  * mysql installed (+ created a schema)
  * node js installed (v.9.2.1)
  * npm installed (5.5.1)

# 2. Edit config.js file with your parameters:
      development: {
		  database
      },
      
# 3. Run the application:
```sh
$ yarn
```
```sh
$ npm start
```
# 4. Request examples:
* GET /persons - return list of persons;

# 5. What will be added/changed in the future:
* First;