const express = require('express');
const morgan = require('morgan');
const config = require('config');
const Routes = require('./src/middleware/Routes');
const logger = require('./src/middleware/Logger');

const app = express();

app.use(morgan('tiny', { stream: logger.stream })); // stream messages from morgan to winston, TODO: can be changed to express-winston

const router = express.Router();

router.use((req, res, next) => {
	req.logger = logger;
	next();
});

Routes.assignRoutes(router);

app.use('/', router);

app.listen(config.port);
