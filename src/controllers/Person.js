const pool = require('../database/Database');

class Person {
	static async getPersons(req, res) {
		req.logger.info('Getting all persons');
		const allClients = await pool.query('select * from person');
		req.logger.info('Query result:', { message: allClients.rows[0].name });
		res.sendStatus(200);
	}
}

module.exports = Person;
