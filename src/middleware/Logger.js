const { createLogger, transports, format } = require('winston');

const logger = createLogger({
	format: format.combine(
		format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss:ms' }),
		format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`),
	),
	transports: [
		new transports.File({
			filename: './logs/service.log',
		}),
		new transports.Console(),
	],
});

logger.stream = {
	write: (info) => {
		logger.info(info);
	},
};

module.exports = logger;
