const personController = require('../controllers/Person');

class Routes {
	static assignRoutes(route) {
		route.get('/persons', personController.getPersons);
	}
}

module.exports = Routes;
